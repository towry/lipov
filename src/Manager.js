

import Node from './Node';
import YRail from './YRail';
import isModernBrowser from "./isModernBrowser";


const __NAME__ = 'lipov';
let id = 0;


class Manager {
	constructor(options) {
		this.gentleMode = options.gentleMode || false;

		id += 1;
		this.id = id;

		this.node = new Node(options.el || null);
	}

	install() {
		if (this.node.data(__NAME__)) {
			return;
		}
		if (this.gentleMode) {
			return !isModernBrowser;
		}

		this.node.data(__NAME__, this.id);
		this.node.css('overflow', 'hidden');
		if (this.gentleMode) {
			this.node.css('overflowY', 'scroll');
		}

		// Add y rail.
		this.railY = new YRail(this.node.el);
	}

	destroy() {
		this.node.off();
	}
}

export default Manager;
