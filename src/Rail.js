
/**
	Copyright 2016 Towry Wang.
*/

export default class Rail {
	constructor(el) {
		this.container = el;
		this.name = 'rail';
	}

	init() {
		this.addRail();
	}

	/**
	 * Add rail element to the container.
	 */
	addRail() {
		var container = this.container;

		var frag = document.createDocumentFragment();

		var rail = document.createElement('div');
		var klass = 'lipov-rail';
		if (this.name !== 'rail') {
			klass += (' lipov-' + this.name);
		}
		rail.className = klass;
		frag.appendChild(rail);

		// Append rail thumb.
		var thumb = document.createElement('div');
		klass = 'lipov-rail-thumb';
		thumb.className = klass;
		rail.appendChild(thumb);

		// Append to container.
		container.appendChild(frag);
	}

	style() {
		throw new Error("Not implemented.");
	}
}
