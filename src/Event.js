

const PIXEL_STEP  = 10;
const LINE_HEIGHT = 40;
const PAGE_HEIGHT = 800;


export default class Event {
	constructor(el) {
		this.el = el;
		this._listeners = {};
	}

	on(name, handler) {
		let el = this.el;
		let self = this;
		let handleEvent = null;

		name = getEventName(name);

		if (isMouseWheelEvent(name)) {
			handleEvent = function (e) {
				return handler.call(self, normalizeMouseEvent(e || window.event, name), el);
			}
		} else {
			handleEvent = function (e) {
				e = e || window.event;
				e.preventDefault = e.preventDefault || function () {
					e.returnValue = false;
				}
				e.stopPropagation = e.stopPropagation || function () {
					e.cancelBubble = true;
				}

				return handler.call(self, e, el);
			}
		}

		if (window.addEventListener) {
			el.addEventListener(name, handleEvent, false);		
			addToArrayOf(this._listeners, name, function () {
				el.removeEventListener(name, handleEvent);
			});

			return;
		}

		if (window.attachEvent) {
			el.attachEvent('on' + name, handleEvent);
			addToArrayOf(this._listeners, name, function () {
				el.detachEvent('on' + name, handleEvent);
			});

			return;
		}

		el['on' + name] = handleEvent;
		addToArrayOf(this._listeners, name, function () {
			delete el['on' + name];
			el['on' + name] = null;
		});
	}

	off(name) {
		name = name || null;
		let listeners = null;

		if (name === null) {
			// Remove all listeners.
			for (var key in this._listeners) {
				listeners = this._listeners[key];
				emptyListeners(listeners);
			}
			return;
		}

		if (!(name in this._listeners)) {
			return;
		}

		listeners = this._listeners[name];
		emptyListeners(listeners);
	}
}


function emptyListeners(listeners) {
	listeners.forEach(function (listener) {
		listener && listener.call(this);
	}.bind(this));
}


function addToArrayOf(obj, name, value) {
	if (!(name in obj)) {
		obj[name] = [];
	}

	obj[name].push(value);
}

function isMouseWheelEvent(name) {
	return name === 'wheel' ||
		name === 'mousewheel' ||
		name === 'DOMMouseScroll';
}

function getEventName(name) {
	if (!isMouseWheelEvent(name)) {
		return name;
	}

	let eventName = 
		isMouseEventSupported('wheel') ? 'wheel' :
		isMouseEventSupported('mousewheel') ? 'mousewheel' : 
		isMouseEventSupported('DOMMouseScroll');

	return eventName;
}


function isMouseEventSupported(eventName) {
	let el = document.createElement('div');
	eventName = 'on' + eventName;
	
	let isSupported = (eventName in el);
	if (!isSupported) {
		el.setAttribute(eventName, 'return;');
		isSupported = typeof el[eventName] == 'function';
	}
	el = null;
	return isSupported;
}


function normalizeMouseEvent(e, name) {

	let et = {
		originalEvent: e,
		target: e.target || e.srcElement,
		type: 'wheel',
		deltaMode: e.type === 'MozMousePixelScroll' ? 0 : 1,
		deltaX: 0,
		deltaY: 0,
		deltaZ: 0,
		preventDefault: function () {
			e.preventDefault ? e.preventDefault() : e.returnValue = false;
		},
	};

	let sX = 0, sY = 0,
		pX = 0, pY = 0;

	if (name === 'mousewheel') {
		et.deltaY = - 1 / 40 * e.wheelDelta;
		// webkit support wheelDeltaX
		e.wheelDeltaX && (et.deltaX = - 1 / 40 * e.wheelDeltaX);
	} else {
		et.deltaY = e.detail;
	}

	if ('detail' in e) { sY = e.detail; }
	if ('wheelDelta' in e) { sY = -e.wheelDelta / 120; }
	if ('wheelDeltaY' in e) { sY = -e.wheelDeltaY / 120; }
	if ('wheelDeltaX' in e) { sX = -e.wheelDeltaX / 120; }

	// side scrolling on FF with DOMMouseScroll
	if ('axis' in e && e.axis === e.HORIZONTAL_AXIS) {
		sX = sY;
		sY = 0;
	}

	pX = sX * PIXEL_STEP;
	pY = sY * PIXEL_STEP;

	if ('deltaY' in e) { pY = e.deltaY; }
	if ('deltaX' in e) { pX = e.deltaX; }

	if ((pX || pY) && e.deltaMode) {
		if (e.deltaMode == 1) {
			pX *= LINE_HEIGHT;
			pY *= LINE_HEIGHT;
		} else {
			pX *= PAGE_HEIGHT;
			pY *= PAGE_HEIGHT;
		}
	}

	if (pX && !sX) { sX = (pX < 1) ? -1 : 1; }
	if (pY && !sY) { sY = (pY < 1) ? -1 : 1; }

	if (name === 'wheel') {
		et = e;
	}

	et.spinX = sX;
	et.spinY = sY;
	et.pixelX = pX;
	et.pixelY = pY;

	return et;
}
