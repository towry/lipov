

var uaInfo = (function(){
	var ua = navigator.userAgent, tem, 
	M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	if(/trident/i.test(M[1])){
		tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
		return 'IE '+ (tem[1] || '');
	}
	if(M[1] === 'Chrome'){
		tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
		if(tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	}
	M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	if ((tem = ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	return M;
})();

/** 
 * Check if the current browser support styling scrollbar.
 * Support reports from caniuse.com:
 *    Chrome: 49
 *    Safari: 9.1
 * So, we only check those two browsers, for other browsers, just set
 * it to false.
 */
var isModernBrowser = (function () {
	if (uaInfo.length < 2) {
		return false;
	}

	var br = uaInfo[0].toLowerCase();
	var ver = parseInt(uaInfo[1], 10) || 0;

	if ((br !== 'chrome' && br !== 'safari') 
		|| ver <= 0) {
		return false;
	}

	if (br === 'chrome' && ver < 49) {
		return false;
	}

	if (br === 'safari' && ver < 9.1) {
		return false;
	}

	return true;
})();


export default isModernBrowser;
