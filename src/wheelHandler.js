
import updateScroll from '../updateScroll';

export default function (e, el) {
	var pixelX = e.pixelX,
		pixelY = e.pixelY;

	var valueX, valueY;

	if (!pixelX && !pixelY) {
		return;
	}

	valueX = el.scrollLeft;
	valueY = el.scrollTop;

	valueX += pixelX;
	valueY += pixelY;

	if (valueX <= 0) {
		valueX = 0;
	}
	if (valueY <= 0) {
		valueY = 0;
	}

	updateScroll(el, valueX, valueY);
}
