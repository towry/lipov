
import Manager from './Manager';

export default function (el, gentleMode) {
	var manager = new Manager({
		el: el,
		gentleMode: gentleMode
	});

	manager.install();

	return {
		destroy() {
			return manager.destroy();
		}
	}
}
