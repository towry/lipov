
import Event from './Event';

export default class Node {
	constructor(el) {
		if (!isHtmlNode(el)) {
			throw new TypeError("Expected a HTML element.");
		}

		this.el = el;
		this.ownerDocument = this.el.ownerDocument || document;
		this.event = new Event(this.el);
	}

	data(name, value) {
		if (typeof value === 'undefined' || value === null) {
			return getEleData(this.el, name);
		} else {
			setEleData(this.el, name, value);
		}
	}

	on(name, handler) {
		return this.event.on(name, handler);
	}

	off(name) {
		return this.event.off(name);
	}

	css(name, value) {
		this.el.style[name] = value;
	}
}


function isHtmlNode(node) {
	try {
		return node instanceof HTMLElement;
	} catch (e) {
		return (typeof node === 'object') &&
		(node.nodeType === 1) && (typeof node.style === 'object') &&
		(typeof node.ownerDocument === 'object');
	}
}


function getEleData(el, name) {
	if (!('dataset' in document.body)) {
		return el.getAttribute('data-' + name) || null;
	}

	return el.dataset[name] || null;
}


function setEleData(el, name, value) {
	if (!('dataset' in document.body)) {
		el.setAttribute('data-' + name, value);
	}

	el.dataset[name] = value;
}
