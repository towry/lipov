
/**
	Copyright 2016 Towry Wang.
*/

import Rail from './Rail';

export default class YRail extends Rail {
	constructor(el) {
		super(el);
		this.name = 'yrail';
		this.init();
	}
}
